# 🍤imgtogitee

- 一个用于方便在 Markdown 中插入图片的扩展，支持将图片存放在第三方的图床或对象存储。
- 一个用于方便在 Markdown 中插入随机表情的扩展，文档变得更好看了呢。
- 一般截图软件会把图片放到`剪切板`中，`imgtogitee`原理是把剪切板中的图片存到临时文件夹，然后再上传到`gitee`，最后把返回的图片URL插入到`markdown`文本中，提高写作效率。

## 🍟命令|快捷键

| `名称`                  | `快捷键`       | `描述`                  |
| :---------------------- | :------------- | :---------------------- |
| `imgtogitee.getemo`     | `ctrl+numpad1` | `添加随机表情，如🍓`     |
| `imgtogitee.paste`      | `ctrl+numpad2` | `上传剪切板图片到gitee` |
| `imgtogitee.pasteLocal` | `ctrl+numpad3` | `上传本地板图片到gitee` |


## 🥙插件配置项

| `配置项名称`             | `描述`             | `默认值` |
| :----------------------- | :----------------- | :------- |
| `imgtogitee.gitee_token` | `gitee 的token`    | `空`     |
| `imgtogitee.gitee_url`   | `gitee 的仓库地址` | `空`     |

- gitee_token:自行百度注册
- gitee_url:例如，https://gitee.com/api/v5/repos/kudsu/imgtogitee/contents/imgs/
  - 参数：https://gitee.com/api/v5/repos/{owner}/{repo}/contents/{path}
    - owner：仓库所属空间地址(企业、组织或个人的地址path)，`kudsu`
    - repo：仓库路径(path)，`imgtogitee`
    - path：文件的路径，`imgs`


## 🎇记录遇到的问题
### 1、🥃如何新建项目并打开？
```
npm install -g yo generator-code
```
```
yo code
```
### 2、🍁请求网络数据，为什么输出为空？
`大多是异步的问题，用awite，等有返回值了再使用`

### 3、🍫如何生成一个独立的控制台？
```
let log = vscode.window.createOutputChannel("sober-monkey/log");
log.show();
log.appendLine(data);
```
### 4、🍧如何获取前切板TXT？
```
vscode.env.clipboard.readText().then((text)=>{
    clipboard_content = text; 
    /* code */
});
```
或者
```
let clipboard_content = await vscode.env.clipboard.readText(); 
/* code */
```
### 5、🥣如何在插入文本

```
function insertToEnd(text: string, type?: number): Promise<boolean> {
    if (type === undefined) { type = InsertType.cursor; };
    return new Promise((resolve, reject) => {
        const editor = vscode.window.activeTextEditor;
        if (editor) {
            editor.edit(editBuilder => {
                switch (type) {
                    case InsertType.start:
                        editBuilder.insert(new vscode.Position(0, 0), text);
                        break;
                    case InsertType.end:
                        let linenumber = vscode.window.activeTextEditor?.document.lineCount || 1;
                        let pos = vscode.window.activeTextEditor?.document.lineAt(linenumber - 1).range.end || new vscode.Position(0, 0);
                        editBuilder.insert(pos, text);
                    break;
                    case InsertType.cursor:
                        editBuilder.insert(editor.selection.active, text);
                        break;
                    default:
                        break;
                }
                
            }).then(resolve);
        }
    });
}
enum InsertType {
    start = 1,
    end = 2,
    cursor=3
}
```

### 6、🐭PowerShell - 使用Windows截图并在cmd中草绘剪贴板临时图像
```
Add-Type -AssemblyName System.Windows.Forms
$clipboard = [System.Windows.Forms.Clipboard]::GetDataObject()
if ($clipboard.ContainsImage()) {
    $filename='c:\temp\test3.png'         
    [System.Drawing.Bitmap]$clipboard.getimage().Save($filename, [System.Drawing.Imaging.ImageFormat]::Png)
    Write-Output "clipboard content saved as $filename"
} else {
    Write-Output "clipboard does not contains image data"
}

##下面也可以
#get-clipboard -format image
#$img = get-clipboard -format image
#$img.save("c:\temp\temp.jpg")
```

### 7、🛷vscode 设置、读取配置
```
"contributes": {
	"configuration": {
		"type": "object",
		// 显示在配置页左侧
		"title": "Code插件demo",
		"properties": {
			// 全局唯一的配置ID
			"vscodePluginDemo.yourName": {
				"type": "string",
				"default": "guest",
				"description": "你的名字"
			},
			"vscodePluginDemo.showTip": {
				"type": "boolean",
				"default": true,
				"description": "启动时显示自定义欢迎页"
			}
		}
	}
}

```
```
// 如果没有设置，返回undefined
const result = vscode.workspace.getConfiguration().get('vscodePluginDemo.yourName');
```

**🥈Enjoy!**
