import type { AxiosInstance, AxiosRequestConfig } from "axios";
import axios from "axios";

class Http {
  private readonly options: AxiosRequestConfig;
  private axiosInstance: AxiosInstance;

  // 构造函数 参数 options
  constructor(params: AxiosRequestConfig) {
    this.options = params;
    this.axiosInstance = axios.create(params); // 生成实例
    this.setupInterceptors();
  }
  private setupInterceptors() {
    this.axiosInstance.defaults.baseURL = "/";
    this.axiosInstance.defaults.headers.post["Content-Type"] =
      "application/json";
    this.axiosInstance.interceptors.request.use(
      (config) => {
        if (!config.headers) {
          config.headers = {};
        }
        // config.headers.Authorization = CSRF_TOKEN;
        return config;
      },
      () => {
        return Promise.reject({
          code: 1,
          message: "请求错误，请联系管理员",
        });
      }
    );

    this.axiosInstance.interceptors.response.use(
      (response) => {
        return Promise.resolve(response);
      },
      (error) => {
        let message = "";
        if (error.response) {
          switch (error.response.status) {
            case 400:
              message = "请求错误(400)";
              break;
            case 401:
              message = "无效的token(401)";
              break;
            case 403:
              message = "拒绝访问(403)";
              break;
            case 404:
              message = "请求出错(404)";
              break;
            case 408:
              message = "请求超时(408)";
              break;
            case 500:
              message = "服务器错误(500)";
              break;
            case 501:
              message = "服务未实现(501)";
              break;
            case 502:
              message = "网络错误(502)";
              break;
            case 503:
              message = "服务不可用(503)";
              break;
            case 504:
              message = "网络超时(504)";
              break;
            case 505:
              message = "HTTP版本不受支持(505)";
              break;
            default:
              message = "未知错误，请联系管理员";
              break;
          }
        } else {
          if (error.code && error.code === "ECONNABORTED") {
            message = "请求超时，请检查网是否正常";
          } else {
            message = "未知错误，请稍后再试";
          }
        }
        return Promise.reject({
          code: -1,
          message: message,
        });
      }
    );
  }
  /**
   * Http get
   * @param url 请求路径
   * @param config 配置信息
   * @returns Promise
   */
  get(url: string, config?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.axiosInstance
        .get(url, config)
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
  /**
   * Http post
   * @param url 请求路径
   * @param data 请求数据
   * @param config 配置
   * @returns Promise
   */
  post(url: string, data?: any, config?: any): Promise<any> {
    return new Promise((reslove, reject) => {
      this.axiosInstance
        .post(url, data, config)
        .then((response) => {
          reslove(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
const http = new Http({
  timeout: 1000 * 5,
});
export default http;
